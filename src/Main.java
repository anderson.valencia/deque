import java.util.ArrayDeque;
import java.util.Deque;

public class Main {

    public static void main(String[] args) {
        // Creamos un nuevo ArrayDeque
        Deque<String> deque = new ArrayDeque<>();

        // Podemos añadir elementos a la cola de diferentes maneras

        // Añadirlos al final
        deque.add("Elemento 1 (Cola)");

        // Añadirlos al inicio
        deque.addFirst("Elemento 2 (Cabeza)");

        // Añadirlos al final
        deque.addLast("Elemento 3 (Cola)");

        // Añadirlos al inicio
        deque.push("Elemento 4 (Cabeza)");

        // Añadirlo al final
        deque.offer("Elemento 5 (Cola)");

        // Añadirlo al inicio
        deque.offerFirst("Elemento 6 (Cabeza)");

        System.out.println(deque + "\n");

        // Podemos eliminar el primer o el último elemento
        deque.removeFirst();
        deque.removeLast();
        System.out.println("Deque después de eliminar el primer elemento y el último elemento:  " + deque);

        deque.removeFirst();
        deque.removeLast();
        System.out.println("Deque después de eliminar (una vez más) el primer elemento y el último elemento:  " + deque);
    }


}


